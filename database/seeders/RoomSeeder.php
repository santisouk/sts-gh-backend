<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rooms')->insert([
            [
                'room' => 'ຫ້ອງເບີ 1',
                'type_of_room' => 'ແອ',
                'price' => 100000,
                'picture' => '',
                'room_status' => 'ຫວ່າງ',
            ],
            [
                'room' => 'ຫ້ອງເບີ 2',
                'type_of_room' => 'ພັດລົມ',
                'price' => 60000,
                'picture' => '',
                'room_status' => 'ບໍ່ຫວ່າງ',
            ]
        ]);
    }
}
