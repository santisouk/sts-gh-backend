<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->id('id');
            $table->string('room', 20);
            $table->enum('type_of_room',['ແອ', 'ພັດລົມ']);
            $table->double('price', 15);
            $table->string('picture', 15)->nullable();
            $table->enum('room_status', ['ຫວ່າງ', 'ບໍ່ຫວ່າງ', 'ສ້ອມແຊມ']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
