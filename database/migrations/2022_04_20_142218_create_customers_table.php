<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('fullname',30);
            $table->string('nationality',15);
            $table->date('date_of_birth');
            $table->text('address');
            $table->string('passport','30')->nullable();
            $table->string('id_card','10')->nullable();
            $table->string('issue_at','15');
            $table->string('tel','15');
            $table->string('entering_of_border','30')->nullable();
            $table->enum('gender', ['f', 'm']);
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
