<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'title',
        'firstname',
        'lastname',
        'nickname',
        'address',
        'birth_date',
        'tel',
        'email',
        'user_id'
    ];


}
