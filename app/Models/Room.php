<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    const STATUS_AVAILABLE = "ຫວ່າງ";
    protected $fillable = ['room', 'type_of_room', 'picture', 'room_status', 'price'];


}
