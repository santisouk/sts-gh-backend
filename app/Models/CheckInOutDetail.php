<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CheckInOutDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'date_in',
        'date_out',
        'comming_of_purpose',
        'check_in_out_id',
        'room_id',
    ];



    public function checkInOut()
    {
        return $this->belongsTo(CheckInOut::class, 'check_in_out_id', 'id');
    }
    public function room()
    {
        return $this->belongsTo(Room::class, 'room_id');
    }
}
