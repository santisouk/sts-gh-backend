<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CheckInOut extends Model
{
    use HasFactory;

    protected $fillable = [
        'sub_total',
        'discount',
        'grand_total',
        'customer_id',
        'employee_id',
        'status'
    ];

    public function checkInOutDetails()
    {
        return $this->hasMany(CheckInOutDetail::class, 'check_in_out_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }
}
