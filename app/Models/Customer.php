<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'fullname',
        'nationality',
        'date_of_birth',
        'address',
        'passport',
        'id_card',
        'issue_at',
        'tel',
        'entering_of_border',
        'gender',
        'user_id'
    ];
}
