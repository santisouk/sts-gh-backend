<?php

namespace App\Models;

use App\Models\Room as ModelsRoom;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    use HasFactory;

    protected $fillable = [
        "asset_name",
        "price",
        "status",
        "room_id",
    ];

    public function room()
    {
        return $this->belongsTo(ModelsRoom::class, 'room_id');
    }
}
