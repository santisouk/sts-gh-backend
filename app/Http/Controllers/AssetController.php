<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\TryCatch;
use App\Helpers\ResponseObject;
use App\Models\Asset;

class AssetController extends Controller
{
    protected $response;

    public function __construct()
    {
        $this->response = new ResponseObject();
    }



    public function index()
    {
        try {
            $assets = Asset::with('room')->get();
            return $this->response->responseSuccess('Fetch data successfully.', $assets);
        } catch (Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }

    public function store(Request $request)
    {
        try {
            $asset = new Asset();
            $asset->asset_name = $request->asset_name;
            $asset->price = $request->price;
            $asset->status = "ໃຊ້ງານ";
            $asset->room_id = $request->room_id;
            $asset->save();
            return $this->response->responseSuccess('SUCCESS', null);
        } catch (Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $asset = Asset::where('id', $id)->update([
                "asset_name" => $request->asset_name,
                "price" => $request->price,
                "status" => $request->status,
                "room_id" => $request->room_id
            ]);
            return $this->response->responseSuccess('Updated successfully', null);
        } catch (Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }


    public function show($id)
    {
        try{
            $asset = Asset::with('room')->where('assets.id', $id)->first();
            return $this->response->responseSuccess('SUCCESS', $asset);
        }
        catch(Exception $exception){
            return $this->response->responseErrors($exception);
        }
    }


    public function destroy($id)
    {
        try{
            $asset = Asset::find($id);
            $asset->delete();
            return $this->response->responseSuccess('SUCCESS', null);
        }
        catch(Exception $exception){
            return $this->response->responseErrors($exception);
        }
    }

}
