<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use Exception;
use Illuminate\Http\Request;
use App\Helpers\ResponseObject;
use App\Models\BookingDetail;
use App\Models\Room;
use Illuminate\Support\Facades\DB;

class BookingController extends Controller
{

    protected $response;

    public function __construct()
    {
        $this->response = new ResponseObject();
    }

    public function index(Request $request)
    {
        try {
            $bookings = Booking::with(['bookingDetail' => function($q){
                $q->with('room');
            }, 'customer'])
                // ->whereBetween('bookingFrom', [$request->startDate,  $request->endDate])
                ->get();
            return $this->response->responseSuccess('SUCCESS', $bookings);
        } catch (Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }

    public function show($id)
    {
        try {
            $bookings = Booking::with(['bookingDetail' => function($q){
                $q->with('room');
            }, 'customer'])
            ->where('id', $id)
                ->first();
            return $this->response->responseSuccess('SUCCESS', $bookings);
        } catch (Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }


    public function approve($id)
    {
        try {
            $bookings = Booking::where('id', $id)->update(['status' => 'ອະນຸມັດແລ້ວ']);
            return $this->response->responseSuccess("ລາຍການຈອງ $id  ອະນຸມັດແລ້ວ", null);
        } catch (Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }

    public function bookingByCustomer(Request $request){
        // try {
        //     $bookings = DB::table('bookings')
        //         ->join('')
        //         ->where('customer_id', $request->id)
        //         ->get();
        //     return $this->response->responseSuccess('SUCCESS', $bookings);
        // } catch (Exception $exception) {
        //     return $this->response->responseErrors($exception);
        // }
        try {
            $bookings = Booking::with(['bookingDetail' => function($q){
                $q->with('room');
            }, 'customer'])
                ->where('customer_id', $request->id)
                ->get();
            return $this->response->responseSuccess('SUCCESS', $bookings);
        } catch (Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }



    public function searchBooking(Request $request){
        try {
            if($request->searchText){
                $bookings = Booking::with(['bookingDetail' => function($q){
                    $q->with('room');
                }, 'customer'])
                    ->where('id', $request->searchText)
                    ->orderBy("id", "DESC")
                    ->get();
            }else{
                $bookings = Booking::with(['bookingDetail' => function($q){
                    $q->with('room');
                }, 'customer'])
                    ->orderBy("id", "DESC")
                    ->get();
            }

            return $this->response->responseSuccess('SUCCESS', $bookings);
        } catch (Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }

    public function findRoom(Request $request){
        try {
            $bookings = DB::table('bookings')
                ->join('booking_details', "booking_details.booking_id", "=", "bookings.id")
                ->join('rooms', "rooms.id", "=", "booking_details.room_id")
                ->whereBetween('bookingFrom', [$request->startDate,  $request->endDate])
                ->orWhereBetween('bookingTo', [$request->startDate,  $request->endDate])
                ->get();

             $notAvailRoom = [];

             if(count($bookings)>0){
                foreach($bookings as $key => $row){
                    array_push($notAvailRoom, $row->room_id);
                }
                $room = Room::whereNotIn('id', $notAvailRoom)->get();
             }else{
                $room = Room::all();
             }
            //  return $notAvailRoom;
            return $this->response->responseSuccess('SUCCESS',  $room);
        } catch (Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }

    public function book(Request $request){
        DB::beginTransaction();
        try {

            $booking = new Booking();
            $booking->customer_id = $request->customerId;
            $booking->bookingFrom = $request->bookingFrom;
            $booking->bookingTo = $request->bookingTo;
            $booking->pledge = $request->pledge;
            $booking->employee_id = $request->employeeId;
            $booking->status = 'ລໍຖ້າອະນຸມັດ';
            $booking->save();

            $rows = $request->bookingItems;
            $check_in_out_list = [];
            foreach ($rows as $key => $row) {
                $check_in_out_list[] = [
                    'booking_id' => $booking->id,
                    'room_id' => $row['roomId'],
                ];
            }

            $booking_detail = BookingDetail::insert($check_in_out_list);
            DB::commit();
            return $this->response->responseSuccess('ຈອງສຳເລັດ ລໍຖ້າອານຸມັດ', null);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->response->responseErrors($exception);
        }
    }

}
