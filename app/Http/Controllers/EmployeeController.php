<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Helpers\ResponseObject;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class EmployeeController extends Controller
{
    protected $response;

    public function __construct()
    {
        $this->response = new ResponseObject();
    }

    public function index()
    {
        try {
            $employees = Employee::all();
            return $this->response->responseSuccess('success', $employees);
        } catch (Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {

        try {

            $user = User::create([
                'name' => $request->username,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'role' => 'employee'
            ]);


            $employee  = Employee::create([
                'title' =>  $request->title,
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
                'nickname' => $request->nickname,
                'address' => $request->address,
                'birth_date' => $request->birth_date,
                'tel' => $request->tel,
                'email' => $request->email,
                'user_id' => $user->id,
            ]);
            return $this->response->responseSuccess('success', $employee);
        } catch (Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }


    public function show($id)
    {
        try {
            $employee = Employee::where('id', $id)->first();
            return $this->response->responseSuccess('success', $employee);
        } catch (Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }

    public function edit(Employee $employee)
    {
        //
    }

    public function update(Request $request, $id)
    {

        try {
            $employee  = Employee::where('id', $id)->update([
                'title' =>  $request->title,
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
                'nickname' => $request->nickname,
                'address' => $request->address,
                'birth_date' => $request->birth_date,
                'tel' => $request->tel,
                'email' => $request->email,
            ]);
            return $this->response->responseSuccess('updated successfully', null);
        } catch (Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }


    public function destroy(Employee $employee)
    {
        //
    }
}
