<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseObject;
use App\Models\Room;
use Exception;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    protected $response;

    public function __construct()
    {
        $this->response = new ResponseObject();
    }


    public function destroy($id)
    {
        try {
            $room = Room::find($id);
            $room->delete();
            return $this->response->responseSuccess('Delete data successfully', null);
        } catch (Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }

    public function show($id)
    {
        try {
            $room = Room::where("id", $id)->first();
            return $this->response->responseSuccess('Fetched data successfully', $room);
        } catch (Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }

    public function index()
    {
        try {
            $rooms = Room::all();
            return $this->response->responseSuccess('Fetched room successfully', $rooms);
        } catch (\Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }

    public function store(Request $request)
    {
        try {
            Room::create([
                'room' => $request->room,
                'type_of_room' => $request->type_of_room,
                'price' => $request->price,
                'status' => Room::STATUS_AVAILABLE,
            ]);
            return $this->response->responseSuccess('Inserted succesfully', null);
        } catch (\Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }

    public function update($id, Request $request)
    {
        try {
            $room = Room::where('id', $id)->update([
                'room' => $request->room,
                'type_of_room' => $request->type_of_room,
                'price' => $request->price,
                'room_status' => $request->room_status

            ]);
            return $this->response->responseSuccess('Updated succesfully', null);
        } catch (\Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }
}
