<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseObject;
use App\Models\Customer;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
    protected $response;

    public function __construct()
    {
        $this->response = new ResponseObject();
    }

    public function index()
    {
        try {
            $customers = Customer::all();
            return $this->response->responseSuccess('SUCCESS', $customers);
        } catch (Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }


    public function show($id)
    {
        try {
            $customer = Customer::find($id);
            return $this->response->responseSuccess('SUCCESS', $customer);
        } catch (Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }

    public function fetchCustomerByUser($id)
    {
        try {
            $customer = Customer::where('user_id', $id)->first();
            return $this->response->responseSuccess('SUCCESS', $customer);
        } catch (Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }


    public function store(Request $request)
    {

        try {
            DB::beginTransaction();

            $user = User::create([
                'name' => $request->username,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'role' => 'customer'
            ]);

            $customer  = Customer::create([
                'fullname' => $request->fullname,
                'nationality' => $request->nationality,
                'date_of_birth' => $request->date_of_birth,
                'address' => $request->address,
                'passport' => $request->passport,
                'id_card' => $request->id_card,
                'issue_at' => $request->issue_at,
                'tel' => $request->tel,
                'entering_of_border' => $request->entering_of_border,
                'gender' =>  $request->gender,
                'user_id' => $user->id,
            ]);

            DB::commit();
            return $this->response->responseSuccess('ບັນທຶກຂໍ້ມູນລູກຄ້າສຳເລັດ', null);
        } catch (Exception $exception) {
            DB::rollBack();
            return $this->response->responseErrors($exception);
        }
    }


    public function update(Request $request, $id)
    {

        try {
            $customer  = Customer::where('id', $id)->update([
                'fullname' => $request->fullname,
                'nationality' => $request->nationality,
                'date_of_birth' => $request->date_of_birth,
                'address' => $request->address,
                'passport' => $request->passport,
                'id_card' => $request->id_card,
                'issue_at' => $request->issue_at,
                'tel' => $request->tel,
                'entering_of_border' => $request->entering_of_border,
                'gender' =>  $request->gender
            ]);
            return $this->response->responseSuccess('updated successfully', null);
        } catch (Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }

    public function destroy($id)
    {
        try {
            $customer = Customer::find($id);
            $customer->delete();
            return $this->response->responseSuccess('SUCCESS', null);
        } catch (Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }
}
