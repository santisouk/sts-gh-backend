<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseObject;
use App\Models\CheckInOut;
use App\Models\CheckInOutDetail;
use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CheckInOutController extends Controller
{
    protected $response;

    public function __construct()
    {
        $this->response = new ResponseObject();
    }

    public function checkIn(Request $request)
    {
        DB::beginTransaction();
        try {

            $check_in_out = new CheckInOut();
            $check_in_out->customer_id = $request->customerId;
            $check_in_out->employee_id = $request->employeeId;
            $check_in_out->sub_total = $request->subTotal;
            $check_in_out->discount = $request->discount;
            $check_in_out->grand_total = $request->grandTotal;
            $check_in_out->status = 'ຍັງບໍ່ທັນຊຳລະ';
            $check_in_out->save();

            $rows = $request->checkInDetail;
            $check_in_out_list = [];
            foreach ($rows as $key => $row) {
                $check_in_out_list[] = [
                    'date_in' => $row['dateIn'],
                    'date_out' => $row['dateOut'],
                    'comming_of_purpose' => $request->commingOfPurpose,
                    'check_in_out_id' => $check_in_out->id,
                    'room_id' => $row['selectedRoom'],
                ];
                $room = Room::find($row['selectedRoom']);
                $room->update([
                    'room_status' => 'ບໍ່ຫວ່າງ'
                ]);
            }

            $check_in_out_detail = CheckInOutDetail::insert($check_in_out_list);
            DB::commit();
            return $this->response->responseSuccess('ບັນທຶກຂໍ້ມູນສຳເລັດ', null);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->response->responseErrors($exception);
        }
    }


    public function checkOut(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $check_in_out =  CheckInOut::where('id', $id)->update([
               'customer_id' => $request->customerId,
               'sub_total' => $request->subTotal,
               'discount' => $request->discount,
               'grand_total' => $request->grandTotal,
               'status' => 'ຈ່າຍແລ້ວ'
            ]);



            $rows = $request->checkInDetail;
            $check_in_out_list = [];
            foreach ($rows as $key => $row) {
                $check_in_out_list = CheckInOutDetail::find($row['itemId']);
                $check_in_out_list->update([
                    'date_in' => $row['dateIn'],
                    'date_out' => $row['dateOut'],
                    'room_id' => $row['selectedRoom'],
                ]);

                $room = Room::find($row['selectedRoom']);
                $room->update([
                    'room_status' => 'ຫວ່າງ'
                ]);
            }


            DB::commit();
            return $this->response->responseSuccess('ບັນທຶກຂໍ້ມູນສຳເລັດ', null);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->response->responseErrors($exception);
        }
    }


    public function index()
    {
        try {
            $checkInOut = CheckInOut::with(['checkInOutDetails', 'customer', 'employee'])->get();
            return $this->response->responseSuccess('Fetched succesfully', $checkInOut);
        } catch (\Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }

    public function show($id)
    {
        try {
            $checkInOut = CheckInOut::with([
                'checkInOutDetails' => function ($q) {
                    $q->with('room');
                },
                'customer',
                'employee'
            ])
            ->where('id', $id)
            ->first();
            return $this->response->responseSuccess('Fetched succesfully', $checkInOut);
        } catch (\Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }

    public function printBill(Request $request)
    {
        try {
            $bill = DB::table('check_in_outs')
                ->select("check_in_outs.id AS check_id", "customers.id AS customer_id", "customers.fullname", "grand_total", "check_in_outs.created_at")
                ->join('customers', "customers.id", "=", "check_in_outs.customer_id")
                ->where("fullname", 'LIKE', '%' . $request->search_text . '%')
                ->orWhere("check_in_outs.id", 'LIKE', '%' . $request->search_text . '%')
                ->get();
            return $this->response->responseSuccess('Fetched succesfully', $bill);
        } catch (\Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }


    public function reportCheckInOut(Request $request)
    {
        try {
            $check_in_outs = CheckInOutDetail::with([
                'checkInOut' => function ($q) {
                    $q->with('customer');
                },
                'room'
            ])->whereBetween('date_in', [$request->startDate,  $request->endDate])->get();
            return $this->response->responseSuccess('Fetched sucessfully', $check_in_outs);
        } catch (\Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }
}
