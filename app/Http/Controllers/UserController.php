<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Helpers\ResponseObject;
use Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $response;

    public function __construct()
    {
        $this->response = new ResponseObject();
    }

    public function login(Request $request)
    {
        try {
            $username = $request->username;
            $password = $request->password;

            $user = User::where('name', $username)->first();
            // return $user->password;fa
            // return Hash::check('123123','$2y$10$TDsoVfHcMFMJvGdxdGW8ieu4KNkPBIQVkmlR7x1Jk5zCcIMaLS4dG');
            if ($user && Hash::check($password, $user->password)) {
                $responseData = [
                    'accessToken' => $user->createToken('MyApp')->accessToken,
                    'user' => $user,
                    'auth' => auth()->user()
                ];

                return $this->response->responseSuccess('SUCCESS', $responseData);
            } else {
                return response()->json(
                    [
                        'status' => '401',
                        'message' => 'Unauthorized',
                    ],
                    401
                );
            }
        } catch (\Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }


    public function getMe()
    {
        // if (auth()->check()) {
        //     $user = auth()->user();
        //     return new UserResource($user);
        // }
        // return response()->json(null, 401);

            //returns details
            return response()->json(['authenticated-user' => auth()->user()], 200);
    }


    public function register(Request $request)
    {
        try {
            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make("123"),
            ]);
            return $this->response->responseSuccess('SUCCESS', null);
        } catch (\Exception $exception) {
            return $this->response->responseErrors($exception);
        }
    }
}
