<?php


namespace App\Helpers;


use Exception;

class ResponseObject
{
    public function responseSuccess(string $message = null, $data = null): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'status' => 'success',
            'message' => $message,
            'dataResponse' => $data
        ], 200);
    }


    public function responseEmptyData(): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'status' => 'OK',
            'message' => 'No data found, Please try again.',
            'dataResponse' => null
        ], 200);
    }

    public function responseUnsuccess(string $message = null): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'status' => 'Failed',
            'message' => $message,
            'dataResponse' => null
        ], 400);
    }

    public function responseErrors(Exception $exception): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'status' => 'Errors',
            'message' => 'error with exception',
            'exception_message'=>$exception->getMessage(),
            'dataResponse' => null
        ], 500);
    }
}
