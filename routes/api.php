<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AssetController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\CheckInOutController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoomController;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::group(['prefix' => 'login'], function(){
//   return
//});

Route::post('/login', [UserController::class, 'login']);
Route::post('/register', [UserController::class, 'register']);

Route::get('/unauthorized', function () {
    return response()->json(
        [
            'status' => '401',
            'message' => 'Unauthorized',
        ],
        401
    );
})->name("unauthorized");





// Route::group(['prefix' => 'customer', 'middleware' => ['auth:api']], function () {
Route::group(['prefix' => 'customer'], function () {
    Route::get('/', [CustomerController::class, 'index']);
    Route::get('/{id}', [CustomerController::class, 'show']);
    Route::get('/fetchCustomerByUser/{id}', [CustomerController::class, 'fetchCustomerByUser']);
    Route::post('/', [CustomerController::class, 'store']);
    Route::post('/{id}', [CustomerController::class, 'update']);
    Route::delete('/{id}', [CustomerController::class, 'destroy']);
});


// Route::group(['prefix' => 'asset', 'middleware' => ['auth:api']], function () {
Route::group(['prefix' => 'asset'], function () {
    Route::get('/', [AssetController::class, 'index']);
    Route::get('/show/{id}', [AssetController::class, 'show']);
    Route::post('/update/{id}', [AssetController::class, 'update']);
    Route::post('/store', [AssetController::class, 'store']);
    Route::delete('/{id}', [AssetController::class, 'destroy']);
});

Route::group(['prefix' => 'room'], function () {
    Route::get('/', [RoomController::class, 'index']);
    Route::post('/create', [RoomController::class, 'store']);
    Route::delete('/delete/{id}', [RoomController::class, 'destroy']);
    Route::get('/show/{id}', [RoomController::class, 'show']);
    Route::post('/update/{id}', [RoomController::class, 'update']);
});


Route::group(['prefix' => 'check_in_out'], function () {
    Route::get('/{id}', [CheckInOutController::class, 'show']);
    Route::post('/', [CheckInOutController::class, 'index']);
    Route::post('/checkin', [CheckInOutController::class, 'checkin']);
    Route::post('/checkout/{id}', [CheckInOutController::class, 'checkout']);
    Route::post('/print_bill', [CheckInOutController::class, 'printBill']);
    Route::post('/report_check_in_out', [CheckInOutController::class, 'reportCheckInOut']);
});

Route::group(['prefix' => 'booking'], function () {
    Route::get('/', [BookingController::class, 'index']);
    Route::get('/{id}', [BookingController::class, 'show']);
    Route::get('/approve/{id}', [BookingController::class, 'approve']);
    Route::post('/bookingByCustomer', [BookingController::class, 'bookingByCustomer']);
    Route::post('/searchBooking', [BookingController::class, 'searchBooking']);
    Route::post('/book', [BookingController::class, 'book']);
    Route::post('/find_room', [BookingController::class, 'findRoom']);
    Route::post('/book', [BookingController::class, 'book']);
});

Route::group(['prefix' => 'employee'], function () {
    Route::get('/', [EmployeeController::class, 'index']);
    Route::get('/show/{id}', [EmployeeController::class, 'show']);
    Route::post('/', [EmployeeController::class, 'store']);
    Route::post('/{id}', [EmployeeController::class, 'update']);
});
